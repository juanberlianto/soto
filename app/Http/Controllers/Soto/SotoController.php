<?php

namespace App\Http\Controllers\Soto;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Helper\Helper;
use Validator;
use Mail;

class SotoController extends Controller{

	protected $_request;
    protected $_insertDate;

	public function __construct(Request $request) {
		$this->_request=$request;
        $this->_insertDate=gmdate("Y-m-d H:i:s",time()+60+60*7);
	}

	public function index(){

		return view('site.soto.index');
	}

	public function hadiah(){

		return view('site.soto.hadiah');
	}

	public function syarat(){

		return view('site.soto.syarat');
	}

	public function registrasi(){

		return view('site.soto.registrasi');
	}

	public function pengumpulan(){

		return view('site.soto.pengumpulan');
	}

	public function contact(){

		return view('site.soto.contact');
	}

	public function email()
	{

		$nama=trim($this->_request->input('nama'));
		$telepon=trim($this->_request->input('telepon'));
		$email=trim($this->_request->input('email'));
		$message=trim($this->_request->input('message'));

		$valid = [
			'nama'=>'required|max:100',
			'telepon'=>'required|min:8|max:50',
			'email'=>'required|email|max:255',
			'message'=>'required|max:500'
		];

		$validator = Validator::make($this->_request->all(),$valid);
		if($validator->fails())
		{
			$msg="";
			foreach ($validator->errors()->all() as $item) 
			{
				$msg .= "<li>".$item."</li>";
			}
			return response()->json(['status'=>'0','pesan'=>$msg]);
		}

		try

		{
			$dataInsert=[
				'nama'=>$nama,
				'telepon'=>$telepon,
				'email'=>$email,
				'message'=>$message,
				'tgl_input'=>$this->_insertDate,
				'status'=>1,
			];

			$row=['email'=>$email,'nama'=>$nama];

			Mail::send('site.email.reply', ['row'=>$row], function ($m) use ($row) {

				$m->from('juan_berlianto@summarecon.com', 'Soto Kreasi');
				$m->to($row['email'], $row['nama']);
				$m->subject('Soto Kreasi Competition');

			});

			Mail::send('site.email.receive', ['row'=>$dataInsert], function ($m) use ($row){

				$m->from($row['email'],$row['nama']);
                $m->to('juanberlianto@gmail.com');
                $m->subject('Soto Kreasi');
                $m->cc('juanberlianto16@summarecon.com','Juan');

			});

			return response()->json(['status'=>'1']);
		}
		catch (Exception $e)
		{
			return response()->json(['status'=>'0']);
		}
		
	}
}





