// OVERLAY LOADING
var _baseUrl=$('input[name="base_url"]').val();

overlay('loading');

clean_load();


// SHOW ALERT
var _baseUrl=$('input[name="base_url"]').val();

$(function(){
	$('.register-submit-button').click(function(){
	    overlay('loading','0');
	    $.ajax({
	      url: _baseUrl+'/register',
	      data: $('.form-register-online').serializeArray(),
	      type: 'POST',
	      dataType: 'json',
	      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

	      	success: function(data) {
		        clean_load();
		        if(data.status=='1') {
		          show_alert('success','Submit Berhasil','Mohon cek inbox atau spam Anda.');
		          setTimeout(function(){location.reload(true); }, 9000); 
		        } else {
		          show_alert('warning','Submit Gagal','Mohon untuk mengisi secara lengkap dan benar data yang ada di form untuk melanjutkan proses pada bagian ini. Terima kasih.');
		        }
	      	},

	    	error: function(xhr,status,error) {
	          	clean_load();
	          	alert(error);
	      	}
	    })

	})
})

$(function(){
	$('.contact-submit-button').click(function(){
	    overlay('loading','1950');
	    $.ajax({
	      url: _baseUrl+'/email',
	      data: $('.form-contact-online').serializeArray(),
	      type: 'POST',
	      dataType: 'json',
	      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

	      	success: function(data) {
		        clean_load();console.log(data)
		        if(data.status=='1') {
		          show_alert('success','Submit Berhasil','Mohon cek inbox atau spam Anda.');
		          setTimeout(function(){location.reload(true); }, 9000); 
		        } else {
		          show_alert('warning','Submit Gagal','Mohon untuk mengisi secara lengkap dan benar data yang ada di form untuk melanjutkan proses pada bagian ini. Thank You.');
		        }
	      	},

	    	error: function(xhr,status,error) {
	          	clean_load();
	          	alert(error);
	      	}
	    })

	})
})

// OVERLAY LOADING
function clean_load(){
  	$('.loading').removeAttr('style');
  	$('.loading').html('');         
}

// SHOW ALERT
function show_alert(tipe,title,string){
  	swal({
    	type: tipe,
    	title: title,
    	html: string
  	})
}

// OVERLAY LOADING
function overlay(elm,top) 
{
  	var maskHeight = $(window).height();
  	var maskWidth = $(window).width();
  	$('.'+elm)
  		.css({'width':maskWidth,'height':maskHeight,'position':'absolute','z-index':'99999','background-color':'#FFF','top':top+'px','left':'0px'})
  		.empty()
  		.append('<center><img src="'+_baseUrl+'/assets/images/soto/ajax-loader.gif" id="imgloader"></center>');
  	$('#imgloader').css({'top':(($(window).height())/2),'left':((maskWidth/2)-($('#imgloader').width() / 2)),'position':'absolute'});    
  	$('.'+elm).fadeIn(1000);   
  	$('.'+elm).fadeTo('slow',0.6); 
}   