@extends('layout.layout')
@section('content')

<section id="section1" class="p-b-40 p-t-40">
    <div class="container">
        <div class="row">
            <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-4 animated fadeInUp visible">
                <div class="heading text-left">
                    <h2>SYARAT &<br/>KETENTUAN<br/>KOMPETISI<br>SOTO INDONESIA</h2>
                </div>
            </div>
            <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-8 animated fadeInUp visible">
                <ol class="ol-syarat">
                	<li>Peserta sayembara adalah mahasiswa/i semua jurusan, perorangan atau grup dengan maksimal peserta 3 (tiga) orang.</li>
                	<li>Tidak berlaku untuk karyawan/ti dan keluarga karyawan/ti PT Summarecon Agung Tbk.</li>
                	<li>
                		Setiap peserta wajib membuat 1 (satu) konsep desain booth soto yang terdiri dari :
                		<ul>
                			<li>Layout dan rencana sirkulasi di dalam booth (referensi lihat nomor 5)</li>
							<li>3D</li>
							<li>Gambar tampak, potongan, detil struktur skalatis</li>
							<li>Spesifikasi material termasuk rencana anggaran biaya</li>
							<li>Keterangan format materi desain : format digital gambar dalam bentuk file .jpg</li>
                		</ul>
                	</li>
                	<li>Desain yang diperuntukkan untuk “Booth Soto” dengan ketentuan dasar sebagai berikut : <br>
                		<strong>Booth :</strong>
                		<ul>
                			<li>Luasan total 3 x 4 m</li>
							<li>Tinggi booth 2,5 m</li>
							<li>Muka 4 sisi</li>
							<li>Service door 1 sisi</li>
                		</ul>
                		<strong>Bulkhead :</strong>
                		<ul>
                			<li>Tinggi bulkhead 30 - 40 cm</li>
							<li>Material pelapis bebas</li>
							<li>Signage lighted lettering (LED merek Samsung 3 mata, berwarna putih / daylight)</li>
                		</ul>
                		<strong>Table :</strong>
                		<ul>
                			<li>Tinggi maksimal 120 cm (termasuk peralatan)</li>
							<li>Material pelapis stainless (boleh merupakan material inti maupun sebagai pelapis saja)</li>
                		</ul>
                		<strong>Kebutuhan booth untuk 10 jenis soto :</strong>
                		<ul>
                			<li>Peralatan : 1 freezer, 3 kompor, 1 komputer kasir, 1 tempat peralatan dapur, 1 tempat peralatan makan, area sink</li>
							<li>Area display dan persiapan</li>
							<li>Area serving untuk 3 orang karyawan</li>
                		</ul>
                	</li>
                	<li>Isi formulir pendaftaran secara online di kompetisiboothsoto.jfff.info dengan lengkap sesuai dengan identitas.</li>
                	<li><strong>WAKTU DAN JADWAL KEGIATAN</strong>
                		<ul>
                			<li>Pendaftaran online : 19 Maret - 19 April 2018</li>
							<li>Pengumpulan hasil desain : s/d 30 April 2018 </li>
							<li>Pengumuman pemenang : 6 Mei 2018</li>
                		</ul>
                	</li>
                	<li>Hasil desain dikirimkan melalui :
                		<ul>
                			<li>Melampirkan link Google Drive pada form online di kompetisiboothsoto.jfff.info dengan memilih menu “Pengumpulan Hasil Desain”
							<li>Reply email konfirmasi yang telah Anda dapat setelah melakukan registrasi secara online, berisi link Google Drive. Atau email langsung ke sotoindonesia@summarecon.com dengan judul email “HASIL DESAIN KOMPETISI BOOTH SOTO NO. ID XXXXX” maksimal file 20mb. 
                		</ul>
                	</li>
                	<li>Desain para peserta akan diseleksi oleh dewan juri yang profesional dibidangnya. > Desain para peserta akan diseleksi oleh dewan juri yang profesional dibidangnya dan keputusan juri <strong>tidak dapat diganggu gugat</strong>.</li>
                	<li>Semua hasil karya yang diikutsertakan dalam kompetisi ini akan <strong>menjadi hak milik</strong> PT. Summarecon Agung Tbk. dan dapat digunakan untuk segala keperluan promosi.</li>
                	<li>Desain terpilih dapat dikembangkan atau disesuaikan sesuai kebutuhan oleh PT Summarecon Agung Tbk.</li>
                	<li>Hadiah :<br>1 Pemenang terpilih (perorangan atau kelompok sesuai pendaftaran kepersertaannya) mendapatkan :
                		<ul>
							<li><strong>Uang tunai senilai Rp 30.000.000,- (tiga puluh juta Rupiah).</strong></li>
							<li><strong>Vocer menginap 3D2N di Mövenpick Resort & Spa Jimbaran Bali untuk 1 (satu) kamar.</strong></li>
                		</ul>
                		3 Finalis terpilih (perorangan atau kelompok sesuai pendaftaran kepersertaannya) mendapatkan : Vocer Belanja Mal Kelapa Gading @ Rp 2.000.000,- (dua juta Rupiah).
                	</li>
                	<li><strong class="text-red">KOMPETISI INI TIDAK DIPUNGUT BIAYA, CALON PESERTA HARUS WASPADA TERHADAP UPAYA PENIPUAN YANG MENGATASNAMAKAN PANITIA.</strong></li>
                </ol>

                <div class="checkbox">
                    <label><input value="" type="checkbox"><div class="text-red">Dengan mengikuti kompetisi ini, saya menyatakan bahwa saya menyetujui syarat dan ketentuan yang berlaku serta hasil desain yang saya kirimkan adalah hasil desain orisinil / asli dan belum pernah dilombakan / dipublikasikan sebelumnya.</div></label>
                </div>
    			<div class="checkbox">
                    <label><input value="" type="checkbox"><div class="text-red">Setiap peserta bertanggung jawab atas permasalahan yang ditimbulkan oleh pihak ketiga berkaitan dengan keaslian hasil desain dikemudian hari.</div></label>
                </div>
                <div class="checkbox">
                    <label><input value="" type="checkbox"><div class="text-red">Keputusan dewan juri adalah mutlak dan tidak dapat diganggu gugat. Pihak panitia, dengan alasan tertentu berhak mengubah / menambahkan syarat dan ketentuan serta waktu pelaksanaan melalui pemberitahuan sebelumnya kepada para peserta.</div></label>
                </div>
                <button type="button" class="btn btn-block btn-daftar contact-submit-button">Daftar Sekarang</button>
            </div>
        </div>
    </div>
</section>

@stop



