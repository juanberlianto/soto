@extends('layout.layout')
@section('content')

<!-- SYARAT -->
<section class="p-t-40 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="heading text-left">
                    <h2>PENGUMPULAN<br/>HASIL DESAIN<br>KOMPETISI<br>SOTO INDONESIA</h2>
                    <p class="m-b-0 text-red">Pengumpulan paling lambat 30 April 2018 Pukul 23.59 WIB.</p>
                    <p class="m-b-0 text-red"> Pihak panitia akan menarik data hasil desain para peserta pada 1 Mei 2018.</p>
                </div>
            </div>
            <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-8 animated fadeInUp visible">
                <div id="tabs-003" class="tabs simple">
                    <ul class="tabs-navigation">
                        <li><a href="#Email">Melalui Email</a> </li>
                        <li  class="active"><a href="#Drive">Masukan Link Google Drive</a> </li>
                    </ul>
                    <div class="tabs-content">
                        <div class="tab-pane" id="Email">
                            <p>Reply email konfirmasi pendaftaran Kompetisi Desain Booth Soto Indonesia dengan melampirkan hasil desain maksimal 20 mb atau email langsung ke sotoindonesia@summarecon.com dengan judul email “HASIL DESAIN KOMPETISI BOOTH SOTO NO. ID XXXXX”</p>
                        </div>
                        <div class="tab-pane active" id="Drive">
                            <p>Unggah hasil desain Anda ke Google Drive dan get shareable link-nya lalu isi data dibawah ini :
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Alamat Email</label>
                                            <input type="email" class="form-control required email" name="senderEmail" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="name">Nomor Registrasi</label>
                                            <input type="text" class="form-control required" name="senderName" id="name" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Link Google Drive</label>
                                            <input type="email" class="form-control required email" name="senderEmail" id="email" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                            </form>
                             <div class="checkbox">
                                <label><input value="" type="checkbox"><div class="text-red">Dengan mengikuti kompetisi ini, setiap peserta menyetujui syarat dan ketentuan yang berlaku dan hasil desain yang dikirimkan adalah hasil desain yang ORISINAL / ASLI dan belum pernah dilombakan / dipublikasikan sebelumnya. </div></label>
                            </div>
                            <div class="checkbox">
                                <label><input value="" type="checkbox"><div class="text-red">Setiap peserta bertanggung jawab atas permasalahan yang ditimbulkan oleh pihak ketiga dikemudian hari berkaitan dengan ke-ASLI-an / ke-ORISINAL-an hasil desain.</div></label>
                            </div>
                            <button type="button" class="btn btn-block btn-daftar">Kirim</button>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : SYARAT -->

@stop


