@extends('layout.layout')
@section('content')

<!-- SYARAT -->
<section class="p-t-40 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="heading text-left">
                    <h2>HUBUNGI<br/>KAMI</h2>
                    <p class="m-b-0">SEKRETARIAT JFFF<br>Summarecon Kelapa Gading<br>Jl. Bulevar Kelapa Gading Blok M<br>Jakarta Utara, 14240<br>P: +6221 453 1101<br>F: +6221 450 7982<br>E: sotoindonesia@summarecon.com</p>
                </div>
            </div>
            <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-8 animated fadeInUp visible">
                <form class="form-contact-online">
                    <div class="row" style="border-top: 2px solid;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="upper" for="nama">Nama Lengkap</label>
                                <input type="text" class="form-control required" name="nama" id="nama" aria-required="true">
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="upper" for="telepon">Telepon / Handphone</label>
                                <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="upper" for="email">Alamat Email</label>
                                <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="upper" for="comment">Pesan</label>
                                <textarea type="text" class="form-control required" name="message" rows="6" id="message" aria-required="true"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label><input value="" type="checkbox"><div class="text-red">Saya bukan robot.</div></label>
                            </div>
                            <button type="button" class="btn btn-block btn-daftar contact-submit-button">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>       
        </div>
    </div>
</section>
<!-- END : SYARAT -->

@stop


