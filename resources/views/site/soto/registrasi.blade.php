@extends('layout.layout')
@section('content')

<section id="section1" class="p-b-40 p-t-40">
    <div class="container">
        <div class="row">
            <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-4 animated fadeInUp visible">
                <div class="heading text-left">
                    <h2>REGISTRASI<br/>KOMPETISI<br>SOTO INDONESIA</h2>
                    <p>Isi data diri dengan lengkap & jujur sesuai ID yang terdaftar.</p>
                    <p>Khusus pendaftar grup maksimal 3 (tiga) peserta.</p>
                </div>
            </div>
            <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-8 animated fadeInUp visible">
                

               <div  class="tabs simple">
                   
                    <ul class="tabs-navigation">
                        <li class="active"><a href="#Perorangan"></i>Perorangan</a> </li>
                        <li><a href="#Grup"></i>Grup</a> </li>
                       
                    </ul>
                    <div class="tabs-content">
                        <div class="tab-pane active" id="Perorangan">
                            <form class="form-contact-online">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="nama">Nama Lengkap Peserta</label>
                                            <input type="text" class="form-control required" name="nama" id="nama" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Tempat / Tanggal Lahir</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Pria / Wanita</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Alamat Lengkap</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Alamat Email</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Telephone / Handphone</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Nomor KTP</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                            <input type="file" class="form-control form-control-attach required " name="attachment[]" aria-required="true" accept=".mp4,.jpg,.jpeg,.png,.pdf,.ppt,.pptx,.doc,.docx">
                                        </div>
                                    </div>
                                    

                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Universitas</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Jurusan</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Nomor ID Mahasiswa</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label><input value="" type="checkbox"><div class="text-red">Saya telah membaca dan menyetujui syarat & ketentuan berlaku, serta menjamin keaslian data diri yang didaftarkan</div></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input value="" type="checkbox"><div class="text-red">Saya bukan robot.</div></label>
                                        </div>
                                        <button type="button" class="btn btn-block btn-daftar contact-submit-button">Daftar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="Grup">
                           <ul class="list-inline">
                                <li><span class="badge">1</span></li>
                                <li><h4>Kontak Utama</h4></li>
                            </ul>
                            <form class="form-contact-online">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="nama">Nama Lengkap Peserta</label>
                                            <input type="text" class="form-control required" name="nama" id="nama" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Tempat / Tanggal Lahir</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Pria / Wanita</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Alamat Lengkap</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Alamat Email</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Telephone / Handphone</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Nomor ID</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Universitas</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Jurusan</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Nomor ID Mahasiswa</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <span class="badge">2</span>
                            <form class="form-contact-online">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="nama">Nama Lengkap Peserta</label>
                                            <input type="text" class="form-control required" name="nama" id="nama" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Tempat / Tanggal Lahir</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Pria / Wanita</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Universitas</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Jurusan</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Nomor ID Mahasiswa</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <span class="badge">3</span>
                            <form class="form-contact-online">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="nama">Nama Lengkap Peserta</label>
                                            <input type="text" class="form-control required" name="nama" id="nama" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Tempat / Tanggal Lahir</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Pria / Wanita</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Universitas</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="telepon">Jurusan</label>
                                            <input type="text" class="form-control required" name="telepon" id="telepon" aria-required="true">
                                        </div>
                                    </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Nomor ID Mahasiswa</label>
                                            <input type="email" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label><input value="" type="checkbox"><div class="text-red">Saya telah membaca dan menyetujui syarat & ketentuan berlaku, serta menjamin keaslian data diri yang didaftarkan</div></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input value="" type="checkbox"><div class="text-red">Saya bukan robot.</div></label>
                                        </div>
                                        <button type="button" class="btn btn-block btn-daftar contact-submit-button">Daftar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop



