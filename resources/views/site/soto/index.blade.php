@extends('layout.layout')
@section('content')

<!-- INSPIRO SLIDER -->
<div id="slider" class="inspiro-slider slider-fullscreen arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-overlay-one" style="background-image:url({{asset('assets/images/soto/banner.jpg')}});"></div>
</div>
<!--END : INSPIRO SLIDER -->

<!-- TENTANG SOTO -->
<section id="section-2" class="p-b-40 p-t-40">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="heading text-left">
                    <h2>TENTANG<br/>KOMPETISI<br/>SOTO INDONESIA</h2>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div data-animation-delay="300" data-animation="fadeInUp" class="col-md-6 animated fadeInUp visible">
                        <p class="text-justify">Soto adalah hidangan sederhana yang melambangkan budaya dan kuliner Nusantara. <br><br>Kompetisi desain booth soto diadakan untuk memperkenalkan menu soto ke dunia kuliner Internasional dalam bentuk model percontohan usaha soto yang merupakan program dari BEKRAF untuk mempromosikan Soto "A Spoonful of Indonesian Warmth" ke dunia.
                        </p>
                    </div>

                    <div data-animation-delay="600" data-animation="fadeInUp" class="col-md-6 animated fadeInUp visible">
                        <p class="text-justify">Kompetisi ini terbuka untuk para mahasiswa/i berbakat dari segala jurusan. Menangkan uang tunai puluhan juta Rupiah serta hadiah menarik lainnya.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : TENTANG SOTO -->

<!-- EVENT INFO BOX -->
<section class="p-t-0 p-b-0">
    <div class="event-info-box">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="event-info-icon"><img src="{{asset('assets/images/soto/step-1.svg')}}"></div>
                    <div class="event-info-content">
                        <div class="info-description">Baca syarat & ketentuan yang berlaku.</div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="event-info-icon"><img src="{{asset('assets/images/soto/step-2.svg')}}"></div>
                    <div class="event-info-content">
                        <div class="info-description">Isi form registrasi online dengan lengkap.</div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="event-info-icon"><img src="{{asset('assets/images/soto/step-3.svg')}}"></div>
                    <div class="event-info-content">
                        <div class="info-description">Anda akan menerima email konfirmasi, cek inbox/spam.</div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="event-info-icon"><img src="{{asset('assets/images/soto/step-4.svg')}}"></div>
                    <div class="event-info-content">
                        <div class="info-description">Pengumpulan hasil desain paling lambat 30 April 2018.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : EVENT INFO BOX -->

<!-- INSTAGRAM -->
<section class="p-b-40 p-t-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="m-b-10 text-ig"><a href="https://instawidget.net/v/tag/jfffkurasisoto" id="link-6766ec9196fdae9974517e8eeb3c76aa75b8c3592c8e69defa6947aa96fe2ec1">#jfffkurasisoto</a></h2>
                <!-- LightWidget WIDGET -->
                <script src="//lightwidget.com/widgets/lightwidget.js"></script>
                <iframe src="//lightwidget.com/widgets/197dee335d4754eba6f0e000489c7a0f.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
            </div>
        </div>
    </div>
</section>
<!-- END : INSTAGRAM -->
@stop
   