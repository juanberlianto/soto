@extends('layout.layout')
@section('content')

<!-- Hadiah -->
<section id="hadiah" class="p-b-40 p-t-40">
    <div class="container">
        <div class="row">
            <div data-animation-delay="200" data-animation="fadeInUp" class="col-md-4 animated fadeInUp visible">
                <div class="heading text-left">
                    <h2>HADIAH<br/>KOMPETISI<br/>SOTO INDONESIA</h2>
                </div>
            </div>
            <div data-animation-delay="400" data-animation="fadeInUp" class="col-md-4 animated fadeInUp visible">
            	<p>1 (satu) pemenang terpilih (perorangan atau grup sesuai pendaftaran kepersertaannya) akan mendapatkan :</p>
                <p><strong>Uang tunai senilai <div style="font-size:30px">Rp 30.000.000,-</div> (tiga puluh juta Rupiah)</strong></p>
				
				<p><strong>Voucher menginap 3D2N di Mövenpick Resort & Spa Jimbaran Bali untuk 1 (satu) kamar.</strong></p>
				<div class="separator"></div>
				<p>3 (tiga) finalis terpilih (perorangan atau grup sesuai pendaftaran kepersertaannya) akan mendapatkan :</p>
				<p><strong>Vocer Belanja Mal Kelapa Gading  @ Rp 1.000.000,- (satu juta Rupiah). </strong></p>
				<button type="button" class="btn btn-block btn-daftar">Daftar Sekarang</button>
            </div>
            <div data-animation-delay="600" data-animation="fadeInUp" class="col-md-4 animated fadeInUp visible">
            	<img src="{{asset('assets/images/soto/banner1.jpg')}}" class="img-responsive">
            </div>
            
        </div>
    </div>
</section>
<!-- End : Hadiah -->

@stop