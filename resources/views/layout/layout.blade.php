<!DOCTYPE html>
<html lang="en">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="INSPIRO" />
        <meta name="description" content="Themeforest Template Polo">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Soto Kreasi</title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Cedarville+Cursive" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet"> 
        <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"> 
    </head>

    <body class="side-panel side-panel-static">

        <header id="header" class="dark hidden-lg hidden-md">
            <div id="header-wrap">
                <div class="container"> 
                    <div id="logo">
                        <a href="{{URL::to('/')}}" class="logo">
                            <img src="{{asset('assets/images/soto/logo-soto.png')}}" class="img-responsive">
                        </a>
                    </div>

                    <div class="header-extras">
                        <ul>
                            <li>
                                <a id="side-panel-trigger" href="#" class="toggle-item" data-target="body" data-class="side-panel-active">
                                    <i class="fa fa-bars"></i>
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <!-- Side Panel -->
        <div id="side-panel" class="text-center dark">
            <div id="close-panel">
                <i class="fa fa-close"></i>
            </div>

            <div class="side-panel-wrap">
                <div class="logo">
                    <a href="{{URL::to('/')}}"><img src="{{asset('assets/images/soto/logo-soto.png')}}" class="img-responsive"></a>
                </div>
                
                <div id="mainMenu" class="menu-onclick menu-vertical">
                    <div class="container">
                        <nav>
                            <ul>
                                <li><a href="{{URL::to('/')}}">Beranda</a></li>
                                <li><a class="scroll-to" href="#section-2">Tentang Kami</a></li>
                                <li><a href="{{URL::to('hadiah')}}">Hadiah</a></li>
                                <li><a href="{{URL::to('syarat')}}">Syarat & Ketentuan</a></li>
                                <li><a href="{{URL::to('registrasi')}}">Form Registrasi</a></li>
                                <li><a href="{{URL::to('pengumpulan')}}">Pengumpulan Hasil Design</a></li>
                                <li><a href="{{URL::to('contact')}}">Hubungi Kami</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <hr class="space">
                <!--Social Icons-->
                <div class="social-icons social-icons-colored text-left">
                    <ul>
                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
                <!--End: Social Icons-->
            </div>

        </div>
        <!-- End: Side Panel -->

        <!-- Wrapper -->
        <div id="wrapper">

            @yield('content')

            <footer id="footer" class="footer-light">
                <div class="copyright-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="copyright-text p-t-0 text-left">&copy; Soto Indonesia 2018</div>
                            </div>
                            <div class="col-md-6">
                                <div class="copyright-text p-t-0 text-right">go to <a href="#" class="text-red">jfff.info</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
        <!-- End : Wrapper -->

         <div class="loading"></div>
        <input type="hidden" name="base_url" value="{{ URL::to('') }}">

        <!-- Go To Button -->
        <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
        
        <script src="{{asset('assets/js/jquery.js')}}"></script>
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/functions.js')}}"></script>
        <script src="{{asset('assets/js/custom.js')}}"></script>

    </body>

</html>







